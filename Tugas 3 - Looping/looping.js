//No.1 Looping While
var i = 2;

console.log("LOOPING PERTAMA");
while (i <= 20) {
    console.log(`${i} - I love coding`);
    i += 2;
}

console.log("LOOPING KEDUA");
i -= 2;
while (i >= 2) {
    console.log(`${i} - I will become a mobile developer`);
    i -= 2;
}

//No.2 Looping For
for (var i = 1; i <= 20; i++) {
    if ((i%2) == 1) {
        if (i%3 == 0) {
            console.log(`${i} - I Love Coding`);
        } else {
            console.log(`${i} - Santai`);
        }
    } else if ((i%2) == 0) {
        console.log(`${i} - Berkualitas`);
    }
}

//No.3 Membuat Persegi Panjang # 
for (var i = 1; i <= 4; i++) {
    console.log("########");
}

//No.4 Membuat Tangga
var sum = "";
for (var i = 1; i <= 7; i++) {
    sum += "#";
    console.log(sum);
}

//No.5 Membuat Papan Catur
for (var i = 1; i <= 8; i++) {
    if ((i%2) == 1) {
        console.log(" # # # #");
    } else {
        console.log("# # # # ");
    }
}