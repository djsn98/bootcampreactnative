import React from 'react';
import Router from './src/screens/Router';

const App = () => {
  return <Router />
}

export default App;
