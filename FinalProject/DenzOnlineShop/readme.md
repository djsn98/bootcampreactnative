# Denz Online Shop
Aplikasi penjualan barang online untuk menjual berbagai jenis barang.

## Stack Teknologi

**1. Bagian Frontend**
* React Native
* React Navigation
* Axios
* react-native-picker/picker

**2. Bagian Backend**
* Node.JS
* Express JS

## Rancangan UI
![alt-text-1](./UI_design/login-screen.png "title-1") ![alt-text-2](./UI_design/register-screen.png "title-2") ![alt-text-3](./UI_design/about-screen.png "title-3") ![alt-text-4](./UI_design/home-screen.png "title-4") ![alt-text-5](./UI_design/product-screen.png "title-5") ![alt-text-6](./UI_design/order-screen.png "title-6") ![alt-text-7](./UI_design/after-order-screen.png "title-7")

**Link FIGMA:**
https://www.figma.com/file/aLERZqpeUKV4MAJGFjAAa2/TugasSanber?node-id=0%3A1

## Video Demo Aplikasi
Klik link di bawah:
https://youtu.be/1Zwft09ln7c

## Cara Menjalankan Aplikasi
1. Hubungkan HP dengan kabel data
1. Buka folder dengan VSCode
1. Jalankan terminal
1. Jalankan perintah `npm start`
1. Jalankan perintah `npx react-native run-android`
