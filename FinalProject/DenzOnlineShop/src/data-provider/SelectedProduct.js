import React, { useState, createContext } from 'react';

export const SelectedProductContext = createContext();

export const SelectedProductProvider = ({ children }) => {
    const [selectedProduct, setSelectedProduct] = useState(0);

    return (
        <SelectedProductContext.Provider value={[selectedProduct, setSelectedProduct]}>
            {children}
        </SelectedProductContext.Provider>
    );
}