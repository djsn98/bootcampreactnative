import React, { useContext } from 'react';
import { Text, StyleSheet, View, Image, TextInput, TouchableOpacity, FlatList } from 'react-native';

import { ProductsContext } from '../data-provider/ProductsProvider';
import { ProductsProvider } from '../data-provider/ProductsProvider';

const axios = require('axios').default;

const ProductCard = (item, navigation) => {
    return (
        <TouchableOpacity onPress={() => navigation.navigate('Product', {
            product: item
        })}>
            <View style={{
                height: 220,
                width: 170,
                backgroundColor: '#F8F8F8',
                borderRadius: 13,
                marginLeft: 6.5,
                marginTop: 10

            }}>
                <View style={{ alignItems: 'center', height: 160 }}>
                    <Image style={{ marginTop: 7.5, width: '90%', height: '100%', borderRadius: 13 }} source={{ uri: `data:image/png;base64,${item.productImage}` }} />
                </View>
                <View style={{ width: '90%', marginHorizontal: 10, marginTop: 10 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 17 }}>{item.title}</Text>
                    <Text style={{ fontSize: 16 }}>{item.harga}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

const ProductCards = ({ navigation }) => {
    const [products, setProducts] = useContext(ProductsContext);

    axios.get('https://denz-online-shop-backend-api.herokuapp.com/product')
        .then(function (response) {
            setProducts(response.data)
        })
        .catch(function (error) {
            console.log(error)
        })

    return (
        <FlatList
            data={products}
            renderItem={({ item }) => ProductCard(item, navigation)}
            keyExtractor={item => item.id}
            numColumns={2}
        />
    )
}

const HomeScreen = ({ navigation }) => {

    return (
        <View style={{ height: '100%', backgroundColor: '#E8E8E8' }}>
            <View style={{ height: 60, backgroundColor: '#54BD8A', flexDirection: 'row', paddingVertical: 12, paddingHorizontal: 13 }}>
                <TouchableOpacity onPress={() => navigation.navigate('About')}>
                    <Image
                        style={{ height: 37, width: 37, borderRadius: 50 }}
                        source={require('../assets/images/my-profil.jpg')}
                    />
                </TouchableOpacity>
                <View style={{ marginLeft: 25, backgroundColor: '#F8F8F8', flexDirection: 'row', paddingLeft: 7, borderRadius: 50 }}>
                    <Image style={{ alignSelf: 'center' }} source={require('../assets/images/search-icon.png')} />
                    <TextInput
                        placeholder='Cari barang...'
                        style={{
                            backgroundColor: '#F8F8F8',
                            width: 180,
                            borderTopRightRadius: 50,
                            borderBottomRightRadius: 50,
                        }}
                    />
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('Login')} >
                    <Image
                        style={{ marginLeft: 15 }}
                        source={require('../assets/images/logout-icon.png')}
                    />
                </TouchableOpacity>
            </View>
            <ProductsProvider>
                <ProductCards navigation={navigation} />
            </ProductsProvider>
        </View>
    )
}

const style = StyleSheet.create({

});

export default HomeScreen;