import React from 'react';
import { Text } from 'react-native';
import LoginScreen from './LoginScreen';
import RegisterScreen from './RegisterScreen';
import AboutScreen from './AboutScreen';
import HomeScreen from './HomeScreen';
import ProductScreen from './ProductScreen';
import OrderScreen from './OrderScreen';
import AfterOrderScreen from './AfterOrderScreen';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const Router = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Main">
                <Stack.Screen name='Login' component={LoginScreen} />
                <Stack.Screen name='Register' component={RegisterScreen} />
                <Stack.Screen name='Home' component={HomeScreen} />
                <Stack.Screen name='About' component={AboutScreen} />
                <Stack.Screen name='Product' component={ProductScreen} />
                <Stack.Screen name='Order' component={OrderScreen} />
                <Stack.Screen name='AfterOrder' component={AfterOrderScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default Router;