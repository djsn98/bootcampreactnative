import React from 'react';
import { View, Image, Text, StyleSheet, Touchable, TouchableOpacity } from 'react-native';

const ProductScreen = ({ route, navigation }) => {
    const { product } = route.params;

    return (
        <View style={{ backgroundColor: '#DFDFDF', height: '100%' }}>
            <View style={{ backgroundColor: '#F8F8F8', height: '40%' }}>
                <Image style={{ width: '100%', height: '100%', borderBottomRightRadius: 45, borderBottomLeftRadius: 45 }} source={{ uri: `data:image/png;base64,${product.productImage}` }} />
            </View>
            <View style={{ backgroundColor: '#F8F8F8', paddingLeft: 10, paddingVertical: 10 }}>
                <View>
                    <Text style={{ fontSize: 17, fontWeight: 'bold' }}>{product.title}</Text>
                    <Text style={{ fontSize: 16 }}>{product.harga}</Text>
                </View>
            </View>
            <View style={{ backgroundColor: '#F8F8F8', paddingHorizontal: 10, paddingVertical: 10, marginTop: 3 }}>
                <View>
                    <Text style={{ fontSize: 17, fontWeight: 'bold' }}>Deskripsi</Text>
                    <Text style={{ fontSize: 16 }}>{product.deskripsi}</Text>
                </View>
            </View>
            <View style={{ backgroundColor: '#F8F8F8', paddingHorizontal: 10, paddingVertical: 10, marginTop: 3 }}>
                <View>
                    <Text style={{ fontSize: 17, fontWeight: 'bold' }}>Spesifikasi</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: 10, height: 10, backgroundColor: 'black', borderRadius: 50, marginHorizontal: 10 }}></View>
                        <Text style={{ fontSize: 16 }}>{product.spesifikasi[0]}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: 10, height: 10, backgroundColor: 'black', borderRadius: 50, marginHorizontal: 10 }}></View>
                        <Text style={{ fontSize: 16 }}>{product.spesifikasi[1]}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: 10, height: 10, backgroundColor: 'black', borderRadius: 50, marginHorizontal: 10 }}></View>
                        <Text style={{ fontSize: 16 }}>{product.spesifikasi[2]}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: 10, height: 10, backgroundColor: 'black', borderRadius: 50, marginHorizontal: 10 }}></View>
                        <Text style={{ fontSize: 16 }}>{product.spesifikasi[3]}</Text>
                    </View>
                    {/* <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: 10, height: 10, backgroundColor: 'black', borderRadius: 50, marginHorizontal: 10 }}></View>
                        <Text style={{ fontSize: 16 }}>{item.spesifikasi[]}</Text>
                    </View> */}
                </View>
            </View>
            <View style={{ backgroundColor: '#F8F8F8', height: 115, marginTop: 3, alignItems: 'center', justifyContent: 'center' }}>
                <TouchableOpacity onPress={() => navigation.navigate('Order', {
                    product: product
                })}>
                    <View style={{ width: 200, height: 50, backgroundColor: '#54BD8A', borderRadius: 50, justifyContent: 'center', alignItems: 'center', elevation: 3 }}>
                        <Text style={{ color: '#F8F8F8', fontSize: 17, fontWeight: '700' }}>Beli</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
};

export default ProductScreen;