import React, { useState, useContext } from 'react';
import { View, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import { Picker } from '@react-native-picker/picker';

import { UserContext } from '../data-provider/UsersProvider';
import { UserProvider } from '../data-provider/UsersProvider';

import { ProductsContext } from '../data-provider/ProductsProvider';
import { ProductsProvider } from '../data-provider/ProductsProvider';

const Order = ({ product }) => {
    const [products, setProducts] = useContext(ProductsContext);

    return (
        <View style={{ justifyContent: 'center', backgroundColor: '#F8F8F8', width: '95%', height: 70, marginTop: 5, marginBottom: 10, borderRadius: 10, elevation: 8, }}>
            <View style={{ flexDirection: 'row', marginHorizontal: 7.5 }}>
                <Image style={{ width: 55, height: 55, borderRadius: 10 }} source={{ uri: `data:image/png;base64,${product.productImage}` }} />
                <View style={{ marginLeft: 6 }}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', marginTop: 2 }}>{product.title}</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ fontSize: 15, marginTop: 4 }}>{product.harga}</Text>
                        <Text style={{ fontSize: 15, marginTop: 4, marginLeft: 7 }}>X</Text>
                        <TextInput style={{ marginLeft: 7, paddingVertical: 0.1, fontSize: 15, width: 25, height: 25, marginTop: 2, backgroundColor: '#E4E4E4' }} />
                    </View>
                </View>
                <View style={{ marginLeft: 20 }}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', marginTop: 2 }}>Subtotal</Text>
                    <Text style={{ fontSize: 15, marginTop: 4 }}>Rp.250.000,-</Text>
                </View>
            </View>
        </View>
    )
};

const OrderForm = ({ selectedSender, setSelectedSender, navigation }) => {
    const [user, setUser] = useContext(UserContext);

    return (
        <>
            <View style={{ height: 60, backgroundColor: '#F8F8F8', marginTop: 12, paddingHorizontal: 20, justifyContent: 'center' }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 19 }}>Total Pembelian</Text>
                    <Text style={{ fontSize: 19, marginLeft: 50 }}>:</Text>
                    <Text style={{ fontSize: 17, marginLeft: 15, marginTop: 3 }}>Rp.250.000,-</Text>
                </View>
            </View>
            <View style={{ marginTop: 12, marginHorizontal: 20 }}>
                <Text style={{ fontSize: 19, fontWeight: 'bold' }}>Form Pengiriman</Text>

                <View style={{ marginTop: 12 }}>
                    <Text style={{ fontSize: 17 }}>Nama Penerima</Text>
                    <TextInput style={{ backgroundColor: '#F8F8F8', width: '100%', height: 40, marginTop: 5, marginBottom: 15, borderRadius: 10, paddingHorizontal: 10 }}></TextInput>
                </View>

                <View>
                    <Text style={{ fontSize: 17 }}>Alamat</Text>
                    <TextInput multiline={true} style={{ backgroundColor: '#F8F8F8', width: '100%', height: 80, marginTop: 5, marginBottom: 20, borderRadius: 10, textAlignVertical: 'top', paddingHorizontal: 10 }}></TextInput>
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ fontSize: 17 }}>Pilih Kurir</Text>

                    <View style={{ width: 140, height: 45, backgroundColor: '#F8F8F8', marginLeft: 15, justifyContent: 'center' }}>
                        <Picker
                            selectedValue={selectedSender}
                            onValueChange={(itemValue, itemIndex) =>
                                setSelectedSender(itemValue)
                            }>
                            <Picker.Item label="JNE" value="jne" />
                            <Picker.Item label="JNT" value="jnt" />
                            <Picker.Item label="Sicepat" value="sicepat" />

                        </Picker>
                    </View>

                    <View style={{ marginLeft: 15 }}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Ongkir</Text>
                        <Text style={{ fontSize: 15 }}>Rp.20.000,-</Text>
                    </View>
                </View>
            </View>
            <View style={{ height: 60, backgroundColor: '#F8F8F8', marginTop: 12, paddingHorizontal: 20, justifyContent: 'center' }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 19 }}>Total Pembayaran</Text>
                    <Text style={{ fontSize: 19, marginLeft: 50 }}>:</Text>
                    <Text style={{ fontSize: 17, marginLeft: 15, marginTop: 3 }}>Rp.250.000,-</Text>
                </View>
            </View>
            <View style={{ alignItems: 'center', height: 95, backgroundColor: '#F8F8F8', marginTop: 5, paddingHorizontal: 20, justifyContent: 'center' }}>
                <TouchableOpacity onPress={() => navigation.navigate('AfterOrder')}>
                    <View style={{ width: 200, height: 50, backgroundColor: '#54BD8A', borderRadius: 50, justifyContent: 'center', alignItems: 'center', elevation: 3 }}>
                        <Text style={{ color: '#F8F8F8', fontSize: 17, fontWeight: '700' }}>Order</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </>

    )
};

const OrderScreen = ({ route, navigation }) => {
    const [selectedSender, setSelectedSender] = useState("");

    const { product } = route.params;
    return (
        <View style={{ height: '100%', backgroundColor: '#E8E8E8' }}>
            <View style={{ height: 60, backgroundColor: '#F8F8F8', flexDirection: 'row', alignItems: 'center' }}>
                <Image style={{ marginLeft: 15 }} source={require('../assets/images/back-arrow.png')} />
                <Text style={{ marginLeft: 105, fontSize: 19, fontWeight: 'bold' }}>Order</Text>
            </View>
            <View style={{ backgroundColor: '#D6D6D6', marginTop: 12, marginHorizontal: 12, borderRadius: 10, alignItems: 'center' }}>
                <Text style={{ marginTop: 5, fontSize: 16, fontWeight: 'bold' }}>Order List</Text>
                <ProductsProvider>
                    <Order product={product} />
                </ProductsProvider>
            </View>
            <UserProvider>
                <OrderForm selectedSender={selectedSender} setSelectedSender={setSelectedSender} navigation={navigation} />
            </UserProvider>
        </View >
    )
}

export default OrderScreen