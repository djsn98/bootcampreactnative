import React, { useState, useContext } from 'react';
import { Text, Image, View, StyleSheet, TouchableOpacity } from 'react-native';
import { launchImageLibrary } from 'react-native-image-picker';

import { UserContext } from '../data-provider/UsersProvider';
import { UserProvider } from '../data-provider/UsersProvider';

const PhotoProfile = ({ photo }) => {

    return (
        <Image
            style={styles.photoProfile}
            source={photo}
        />
    )
};

const ProfileCard = () => {
    const [user, setUser] = useContext(UserContext);

    return (
        <>
            <View>
                <View style={{ flexDirection: 'row', marginHorizontal: 30, marginBottom: 7 }}>
                    <View style={{ width: 110 }}>
                        <Text style={{ fontWeight: 'bold' }}>USERNAME</Text>
                    </View>
                    <Text>@djsn98</Text>
                </View>
                <View style={{ height: 1, width: '90%', backgroundColor: '#AAAAAA', alignSelf: 'center' }}></View>
            </View>
            <View>
                <View style={{ flexDirection: 'row', marginHorizontal: 30, marginBottom: 7 }}>
                    <View style={{ width: 110 }}>
                        <Text style={{ fontWeight: 'bold' }}>EMAIL</Text>
                    </View>
                    <Text>jasonden98@gmail.com</Text>
                </View>
                <View style={{ height: 1, width: '90%', backgroundColor: '#AAAAAA', alignSelf: 'center' }}></View>
            </View>
            <View>
                <View style={{ flexDirection: 'row', marginHorizontal: 30, marginBottom: 7 }}>
                    <View style={{ width: 110 }}>
                        <Text style={{ fontWeight: 'bold' }}>NO. TELP</Text>
                    </View>
                    <Text>0859 - 2343 -2344</Text>
                </View>
                <View style={{ height: 1, width: '90%', backgroundColor: '#AAAAAA', alignSelf: 'center' }}></View>
            </View>
        </>
    )
};

const AboutScreen = () => {
    const [photo, setPhoto] = useState(require('../assets/images/upload-photo-icon.jpg'));

    const uploadPhoto = () => {
        let options = {
            noData: true,
        }

        launchImageLibrary(options, (response) => {
            console.log(response)

            if (response.didCancel) {
                console.log('User batal mengambil foto');
                return
            }
            if (response.assets[0].uri) {
                setPhoto({ uri: response.assets[0].uri });
                console.log('test');
            }
        })
    }

    return (
        <View style={styles.screen}>
            <View style={styles.header}>
                <View style={styles.headerContent}>
                    <Text style={styles.headerTitle}>About</Text>
                    <TouchableOpacity onPress={uploadPhoto}>
                        <UserProvider>
                            <PhotoProfile photo={photo} />
                        </UserProvider>
                    </TouchableOpacity>
                    <Text style={styles.profileName}>Dennis Jason</Text>
                </View>
            </View>
            <View style={styles.body}>
                <View style={styles.userProfileInfoCard}>
                    <UserProvider>
                        <ProfileCard />
                    </UserProvider>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    header: {
        flex: 3.5,
        backgroundColor: '#54BD8A',
    },
    body: {
        flex: 3,
        backgroundColor: '#F0F0F0',
    },
    photoProfile: {
        width: 122,
        height: 118,
        borderRadius: 81,
        marginTop: 50,
    },
    headerContent: {
        alignItems: 'center',
    },
    headerTitle: {
        fontFamily: 'Rancho-Regular',
        color: '#FFFFFF',
        fontSize: 80,
        marginTop: 50,
    },
    profileName: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 10,
    },
    profileTitle: {
        color: '#FFFFFF',
    },
    userProfileInfoCard: {
        height: '70%',
        backgroundColor: '#F8F8F8',
        borderRadius: 10,
        elevation: 4,
        marginTop: 20,
        marginBottom: 10,
        marginHorizontal: 15,
        justifyContent: 'space-evenly',
    }

});

export default AboutScreen;