import React, { useContext, useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';

import { UserContext } from '../data-provider/UsersProvider';
import { UserProvider } from '../data-provider/UsersProvider';

const axios = require('axios').default;

const LoginForm = ({ navigation }) => {
    const [user, setUser] = useContext(UserContext);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [authState, setAuthState] = useState(null);

    const onSubmit = (callback) => {

        axios.post('https://denz-online-shop-backend-api.herokuapp.com/login', {
            username: username,
            password: password
        }).then((response) => {
            console.log(response.data);
            if (response.data.authState) {
                setUser(response.data);
                setAuthState(response.data.authState)
                callback();
            } else if (!response.data.authState) {
                setAuthState(response.data.authState);
                console.log('false')
            }
            // setAuthState(!response.data.authState)

        }).catch((error) => {
            console.log(error)
        })
    }


    return (
        <View style={styles.body}>
            <View style={{ paddingHorizontal: 20, flex: 1, marginTop: 25 }}>
                <Text style={{ fontFamily: 'Rancho-Regular', fontSize: 18, marginLeft: 13, marginBottom: 3 }}>Username</Text>
                <TextInput
                    onChangeText={(value) => {
                        console.log(value)
                        setUsername(value)
                    }}
                    style={styles.textBox}
                >
                </TextInput>
                <Text style={{ fontFamily: 'Rancho-Regular', fontSize: 18, marginLeft: 13, marginBottom: 3, marginTop: 20 }}>Password</Text>
                <TextInput onChangeText={(value) => setPassword(value)} style={styles.textBox}></TextInput>
                {authState === false ?
                    <View style={{ alignItems: 'center' }}>
                        <Text style={{ color: 'red' }}>Username/Password salah!</Text>
                    </View>
                    : null
                }
            </View>
            <View style={{ paddingHorizontal: 20, flex: 1, marginTop: 15 }}>
                <TouchableOpacity
                    onPress={() => {
                        const navigateToHome = () => {
                            navigation.navigate('Home');
                            console.log('Berhasil masuk home!')
                        }
                        onSubmit(navigateToHome);
                    }}
                    style={styles.button}>
                    <Text style={styles.buttonLabel}>Sign In</Text>
                </TouchableOpacity>
                <View style={{ alignItems: 'center', flexDirection: 'row', paddingHorizontal: 20, marginVertical: 15 }}>
                    <View style={{ height: 1, flex: 1, backgroundColor: '#737373', marginRight: 3 }}></View>
                    <Text style={{ fontFamily: 'Rancho-Regular' }}>OR</Text>
                    <View style={{ height: 1, flex: 1, backgroundColor: '#737373', marginLeft: 3 }}></View>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('Register')} style={styles.button}>
                    <Text style={styles.buttonLabel}>Register</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const LoginScreen = ({ navigation }) => {

    return (
        <View style={{ flex: 1, backgroundColor: '#54BD8A' }}>
            <View style={styles.header}>
                <Text style={styles.title}>Login</Text>
            </View>
            <UserProvider>
                <LoginForm navigation={navigation} />
            </UserProvider>
        </View>
    )
};

const styles = StyleSheet.create({
    title: {
        color: '#FFFFFF',
        fontSize: 80,
        fontFamily: 'Rancho-Regular',
    },
    header: {
        backgroundColor: '#54BD8A',
        flex: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    body: {
        backgroundColor: '#F0F0F0',
        alignItems: 'stretch',
        height: 440,
        borderTopRightRadius: 50,
        borderTopLeftRadius: 50,
        elevation: 50,
    },
    textBox: {
        flex: 1,
        backgroundColor: '#DAD7D7',
        maxHeight: 50,
        minHeight: 50,
        borderRadius: 50,
        paddingHorizontal: 15,
    },
    button: {
        height: 150,
        backgroundColor: '#54BD8A',
        maxHeight: 50,
        minHeight: 25,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonLabel: {
        color: '#FFFFFF',
        fontSize: 16,
    }
});

export default LoginScreen;