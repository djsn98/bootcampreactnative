import React from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';

const AfterOrderScreen = ({ navigation }) => {
    return (
        <View style={{ height: '100%', alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ marginBottom: 50, fontSize: 30, fontWeight: 'bold' }}>Berhasil Memesan !</Text>
            <Image source={require('../assets/images/order-icon.png')} />
            <View style={{ marginTop: 50, width: 325, backgroundColor: '#A8FCB0', alignItems: 'center' }}>
                <Text style={{ marginTop: 3, fontSize: 17, fontWeight: 'bold' }}>Mohon Transfer ke No. Rek di bawah ini !</Text>
                <Text style={{ marginTop: 5, marginBottom: 3, fontSize: 17 }}>BCA : 12299 3I9 488 238</Text>
            </View>
            <TouchableOpacity onPress={() => navigation.popToTop()}>
                <View style={{ marginTop: 40, width: 200, height: 50, backgroundColor: '#54BD8A', borderRadius: 50, justifyContent: 'center', alignItems: 'center', elevation: 3 }}>
                    <Text style={{ color: '#F8F8F8', fontSize: 17, fontWeight: '700' }}>Selesai</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default AfterOrderScreen;