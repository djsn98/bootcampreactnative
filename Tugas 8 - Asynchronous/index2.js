let readBooksPromise = require('./promise.js');

let books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
];

let promise = readBooksPromise(6000, books[0]);

promise
    .then(function (sisaWaktu) {
        for(let i = 1; i < books.length; i++){
            if(sisaWaktu >= books[i].timeSpent){
                console.log(`saya mulai membaca ${books[i].name}`);
                sisaWaktu -= books[i].timeSpent;
                console.log(`saya sudah selesai membaca ${books[i].name}, sisa waktu saya ${sisaWaktu}`);
            } else if(sisaWaktu < books[i].timeSpent){
                console.log(`saya sudah tidak punya waktu untuk baca ${books[i].name}`);
            }
        }
    })
    .catch(function (error) {
        console.log(error);
    })
