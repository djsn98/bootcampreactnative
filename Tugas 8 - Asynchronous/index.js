let readBooks = require('./callback.js');

let books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
];

function callback(waktu) {
    for(let i = 1; i < books.length; i++) {
        if(waktu > books[1].timeSpent){
            console.log(`saya membaca ${books[i].name}`);

            waktu -= books[i].timeSpent;
            console.log(`saya sudah membaca ${books[i].name}, sisa waktu saya ${waktu}`);

            if (waktu < books[i+1]){
                return
            } 
        }   
    }
    return
}

readBooks(3000, books[0], callback);