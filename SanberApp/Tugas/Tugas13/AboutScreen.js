import React from 'react';
import {Text, Image, View, StyleSheet} from 'react-native';


const AboutScreen = () => {
    return (
        <View style={styles.screen}>
            <View style={styles.header}>
                <View style={styles.headerContent}>
                    <Text style={styles.headerTitle}>About</Text>
                    <Image
                        style={styles.photoProfile} 
                        source={require('./assets/images/my-profil.jpg')}
                    />
                    <Text style={styles.profileName}>Dennis Jason</Text>
                    <Text style={styles.profileTitle}>Backend NodeJs Engineer</Text>
                </View>
            </View>
            <View style={styles.body}>
                <View style={[styles.card, {marginTop: 20, marginBottom: 10, marginHorizontal: 15}]}>
                    <View style={{backgroundColor: '#E0E0E0', flex: 2, borderTopLeftRadius: 10, borderTopRightRadius: 10, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={styles.cardTitle}>Portfolio</Text>
                    </View>
                    <View style={{backgroundColor: '#F8F8F8', flex: 5, borderRadius: 10, alignItems: 'center', justifyContent: 'center'}}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <View style={{alignItems: 'center', marginHorizontal: 40}}>
                                <Image source={require('./assets/images/github-icon.png')} style={{height: 65, width: 65}}/>
                                <Text>@djsn98</Text>
                            </View>
                            <View style={{backgroundColor: '#E0E0E0', width: 1.5, height: 100}}></View>
                            <View style={{alignItems: 'center', marginHorizontal: 40}}>
                                <Image source={require('./assets/images/gitlab-icon.png')} style={{height: 65, width: 70}}/>
                                <Text>@djsn98</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={[styles.card, {marginBottom: 10, marginHorizontal: 15}]}>
                    <View style={{backgroundColor: '#F8F8F8', flex: 2, borderTopLeftRadius: 10, borderTopRightRadius: 10, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={styles.cardTitle}>Kontak</Text>
                    </View>
                    <View style={{backgroundColor: '#E0E0E0', height: 2}}></View>
                    <View style={{backgroundColor: '#F8F8F8', flex: 5, borderRadius: 10}}>
                        <View style={{flexDirection: 'row'}}>
                            <Text>Maaf tidak selesai ka</Text>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    header: {
        flex: 6,
        backgroundColor: '#54BD8A',
        justifyContent: 'center',
    },
    body: {
        flex: 8,
        backgroundColor: '#F0F0F0',
    },
    photoProfile: {
        width: 122,
        height: 118,
        borderRadius: 81,
    },
    headerContent: {
        alignItems: 'center',
        marginTop: 2,
    },
    headerTitle: {
        fontFamily: 'Rancho-Regular',
        color: '#FFFFFF',
        fontSize: 80,
    },
    profileName: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 10,
    },
    profileTitle: {
        color: '#FFFFFF',
    },
    card: {
        flex: 1,
        backgroundColor: '#F8F8F8',
        borderRadius: 10,
        elevation: 4
    },
    cardTitle: {
        fontSize: 35,
        fontFamily: 'Rancho-Regular'
    }
});

export default AboutScreen;