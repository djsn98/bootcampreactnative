import React from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';

const LoginScreen = () => {
    return (
        <View style={{ flex: 1, backgroundColor: '#54BD8A'}}>
            <View style={styles.header}>
                <Text style={styles.title}>Login</Text>
            </View>
            <View style={styles.body}>
                <View style={{paddingHorizontal: 20, flex: 1, marginTop: 25}}>
                    <Text style={{fontFamily: 'Rancho-Regular', fontSize: 18, marginLeft: 13, marginBottom: 3}}>Username</Text>
                    <TextInput style={styles.textBox}></TextInput>
                    <Text style={{fontFamily: 'Rancho-Regular', fontSize: 18, marginLeft: 13, marginBottom: 3, marginTop: 20}}>Password</Text>
                    <TextInput style={styles.textBox}></TextInput>
                </View>
                <View style={{paddingHorizontal: 20, flex: 1, marginTop: 15}}>
                    <TouchableOpacity style={styles.button}>
                        <Text style={styles.buttonLabel}>Sign In</Text>
                    </TouchableOpacity>
                    <View style={{alignItems: 'center', flexDirection: 'row', paddingHorizontal: 20, marginVertical: 15}}>
                        <View style={{height: 1, flex: 1, backgroundColor: '#737373', marginRight: 3}}></View>
                        <Text style={{fontFamily: 'Rancho-Regular'}}>OR</Text>
                        <View style={{height: 1, flex: 1, backgroundColor: '#737373', marginLeft: 3}}></View>
                    </View>
                    <TouchableOpacity style={styles.button}>
                        <Text style={styles.buttonLabel}>Sign Out</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>           
    )
};

const styles = StyleSheet.create({
    title: {
        color: '#FFFFFF',
        fontSize: 80,
        fontFamily: 'Rancho-Regular',
    },
    header: {
        backgroundColor: '#54BD8A',
        flex: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    body: {
        backgroundColor: '#F0F0F0',
        alignItems: 'stretch',
        flex: 8,
        borderTopRightRadius: 50,
        borderTopLeftRadius: 50,
        elevation: 50,
    },
    textBox: {
        flex: 1,
        backgroundColor: '#DAD7D7',
        maxHeight: 50,
        minHeight: 50,
        borderRadius: 50,
    },
    button: {
        flex: 1,
        backgroundColor: '#54BD8A',
        maxHeight: 50,
        minHeight: 25,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonLabel: {
        color: '#FFFFFF',
        fontSize: 16,
    }
});

export default LoginScreen;