import React, { useContext } from 'react';
import { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, KeyboardAvoidingView } from 'react-native';
import { users } from '../data/data';
import { LoginContext } from '../data/provider';

const LoginScreen = ({ screenHandler }) => {
    const [index, setIndex] = useContext(LoginContext);

    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');

    console.log(userName);
    console.log(password);

    return (
        <View style={{ flex: 1, backgroundColor: '#54BD8A' }}>

            <View style={styles.header}>
                <Text style={styles.title}>Login</Text>
            </View>

            <View style={styles.body}>
                <View style={styles.form}>
                    <Text style={styles.input}>Username</Text>
                    <TextInput onChangeText={(value) => setUserName(value)} style={styles.textBox}></TextInput>
                    <Text style={[styles.input, { marginTop: 20 }]}>Password</Text>
                    <TextInput onChangeText={(value) => setPassword(value)} style={styles.textBox}></TextInput>
                </View>
                {/* <KeyboardAvoidingView enabled={false}> */}
                <View style={styles.buttons}>
                    <TouchableOpacity onPress={() => {
                        let x;
                        // console.log(users);
                        for (x in users) {
                            if (users[x].userName == userName) {
                                if (users[x].password == password) {
                                    setIndex(x);
                                    screenHandler('About');
                                }
                            }
                        }
                    }} style={styles.button}>
                        <Text style={styles.buttonLabel}>Sign In</Text>
                    </TouchableOpacity>

                    <View style={styles.divider}>
                        <View style={styles.line}></View>
                        <Text style={styles.or}>OR</Text>
                        <View style={styles.line}></View>
                    </View>

                    <TouchableOpacity onPress={() => screenHandler('Register')} style={styles.button}>
                        <Text style={styles.buttonLabel}>Register</Text>
                    </TouchableOpacity>
                </View>
                {/* </KeyboardAvoidingView> */}
            </View>

        </View>
    )
};

const styles = StyleSheet.create({
    title: {
        color: '#FFFFFF',
        fontSize: 80,
        fontFamily: 'Rancho-Regular',
    },
    header: {
        backgroundColor: '#54BD8A',
        height: 300,
        alignItems: 'center',
        justifyContent: 'center',
    },
    body: {
        backgroundColor: '#F0F0F0',
        alignItems: 'stretch',
        height: 450,
        borderTopRightRadius: 50,
        borderTopLeftRadius: 50,
        elevation: 50,
    },
    textBox: {
        backgroundColor: '#DAD7D7',
        maxHeight: 50,
        minHeight: 50,
        borderRadius: 50,
        paddingHorizontal: 15
    },
    button: {
        height: 50,
        backgroundColor: '#54BD8A',
        maxHeight: 50,
        minHeight: 25,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttons: {
        paddingHorizontal: 20,
        marginTop: 15,
        height: 215
    },
    buttonLabel: {
        color: '#FFFFFF',
        fontSize: 16,
    },
    form: {
        paddingHorizontal: 20,
        flex: 1,
        marginTop: 25
    },
    input: {
        fontFamily: 'Rancho-Regular',
        fontSize: 18,
        marginLeft: 13,
        marginBottom: 3
    },
    divider: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 20,
        marginVertical: 15
    },
    line: {
        height: 1,
        flex: 1,
        backgroundColor:
            '#737373',
        marginRight: 3
    },
    or: {
        fontFamily: 'Rancho-Regular'
    }
});

export default LoginScreen;