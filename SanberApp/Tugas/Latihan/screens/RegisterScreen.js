import React, { useContext } from 'react';
import { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { UsersContext } from '../data/provider';

const RegisterScreen = ({ screenHandler }) => {
    const [users, setUsers] = useContext(UsersContext);

    const [userName, setUserName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [ulangiPassword, setUlangiPassword] = useState('');


    return (
        <View style={styles.screen}>
            <View style={styles.header}>
                <Text style={styles.title}>Register</Text>
            </View>

            <View style={styles.body}>

                <View style={styles.form}>
                    <Text style={styles.label}>Username</Text>
                    <TextInput value={userName} onChangeText={(value) => setUserName(value)} style={styles.textBox}></TextInput>
                    <Text style={[styles.label, { marginTop: 20 }]}>Email</Text>
                    <TextInput value={email} onChangeText={(value) => setEmail(value)} style={styles.textBox}></TextInput>
                    <Text style={[styles.label, { marginTop: 20 }]}>Password</Text>
                    <TextInput value={password} onChangeText={(value) => setPassword(value)} style={styles.textBox}></TextInput>
                    <Text style={[styles.label, { marginTop: 20 }]}>Ulangi Password</Text>
                    <TextInput value={ulangiPassword} onChangeText={(value) => setUlangiPassword(value)} style={styles.textBox}></TextInput>
                </View>

                <View style={styles.buttons}>

                    <TouchableOpacity onPress={() => screenHandler('Login')} style={styles.button}>
                        <Text style={styles.buttonLabel}>Sign In</Text>
                    </TouchableOpacity>

                    <View style={styles.divider}>
                        <View style={[styles.line, { marginRight: 5 }]}></View>
                        <Text style={styles.or}>OR</Text>
                        <View style={[styles.line, { marginLeft: 5 }]}></View>
                    </View>

                    <TouchableOpacity onPress={() => {
                        if (password === ulangiPassword) {
                            setUsers([...users, { userName: userName, email: email, password: password }]);
                            screenHandler('About')
                        } else {
                            console.log("Password yang diulangi tidak sama")
                        }
                    }} style={styles.button}>
                        <Text style={styles.buttonLabel}>Register</Text>
                    </TouchableOpacity>

                </View>

            </View>

        </View>
    )
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#54BD8A'
    },
    title: {
        color: '#FFFFFF',
        fontSize: 80,
        fontFamily: 'Rancho-Regular',
    },
    header: {
        backgroundColor: '#54BD8A',
        height: 150,
        alignItems: 'center',
        justifyContent: 'center',
    },
    body: {
        backgroundColor: '#F0F0F0',
        alignItems: 'stretch',
        height: 585,
        borderTopRightRadius: 50,
        borderTopLeftRadius: 50,
        elevation: 50,
    },
    textBox: {
        flex: 1,
        backgroundColor: '#DAD7D7',
        maxHeight: 50,
        minHeight: 50,
        borderRadius: 50,
        paddingHorizontal: 15,
    },
    buttons: {
        paddingHorizontal: 20,
        flex: 0.5,
        justifyContent: 'flex-end',
        marginBottom: 10
    },
    button: {
        flex: 1,
        backgroundColor: '#54BD8A',
        maxHeight: 50,
        minHeight: 25,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonLabel: {
        color: '#FFFFFF',
        fontSize: 16,
    },
    form: {
        paddingHorizontal: 20,
        flex: 1,
        marginTop: 25
    },
    label: {
        fontFamily: 'Rancho-Regular',
        fontSize: 18,
        marginLeft: 13,
        marginBottom: 3
    },
    divider: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 20,
        marginVertical: 15
    },
    line: {
        height: 1,
        flex: 1,
        backgroundColor: '#737373',

    },
    or: {
        fontFamily: 'Rancho-Regular'
    }
});

export default RegisterScreen;