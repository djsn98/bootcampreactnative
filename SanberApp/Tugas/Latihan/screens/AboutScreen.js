import React, { useContext } from 'react';
import { Text, Image, View, StyleSheet } from 'react-native';
import { UsersContext, LoginContext } from '../data/provider';

const AboutScreen = () => {
    const [index] = useContext(LoginContext);
    const [users] = useContext(UsersContext);

    return (
        <View style={styles.screen}>
            <View style={styles.header}>
                <View style={styles.headerContent}>
                    <Text style={styles.headerTitle}>About</Text>
                    <Image
                        style={styles.photoProfile}
                        source={require('../assets/images/my-profil.jpg')}
                    />
                    <Text style={styles.profileName}>{users[index].userName}</Text>
                    <Text style={styles.profileTitle}>Backend NodeJs Engineer</Text>
                </View>
            </View>
            <View style={styles.body}>
                <View style={styles.portofolioCard}>
                    <View style={styles.portofolioCardHeader}>
                        <Text style={styles.cardTitle}>Portfolio</Text>
                    </View>
                    <View style={styles.portofolioBody}>
                        <View style={styles.portofolioContent}>
                            <View style={styles.portofolioElement}>
                                <Image source={require('../assets/images/github-icon.png')} style={styles.portofolioIcon} />
                                <Text>@djsn98</Text>
                            </View>

                            <View style={styles.headerColor}></View>

                            <View style={styles.portofolioElement}>
                                <Image source={require('../assets/images/gitlab-icon.png')} style={styles.portofolioIcon} />
                                <Text>@djsn98</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.contactCard}>
                    <View style={styles.contactCardHeader}>
                        <Text style={styles.cardTitle}>Kontak</Text>
                    </View>
                    <View style={styles.line}></View>
                    <View style={styles.contactCardBody}>
                        <View style={styles.contentContactCardBody}>
                            <View style={{ height: '100%', justifyContent: 'space-evenly' }}>
                                <Image source={require('../assets/images/wa.png')} />
                                <Image source={require('../assets/images/instagram.png')} />
                                <Image style={{ height: 25, width: 30 }} source={require('../assets/images/mail.png')} />
                            </View>
                            <View style={styles.contactLists}>
                                <Text style={styles.contactList}>0843-3284-2323</Text>
                                <Text style={styles.contactList}>@djsn98</Text>
                                <Text style={styles.contactList}>{users[index].email}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    header: {
        flex: 6,
        backgroundColor: '#54BD8A',
        justifyContent: 'center',
    },
    body: {
        flex: 8,
        backgroundColor: '#F0F0F0',
    },
    photoProfile: {
        width: 122,
        height: 118,
        borderRadius: 81,
    },
    headerContent: {
        alignItems: 'center',
        marginTop: 2,
    },
    headerTitle: {
        fontFamily: 'Rancho-Regular',
        color: '#FFFFFF',
        fontSize: 80,
    },
    profileName: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 10,
    },
    profileTitle: {
        color: '#FFFFFF',
    },
    portofolioCard: {
        flex: 1,
        backgroundColor: '#F8F8F8',
        borderRadius: 10,
        elevation: 4,
        marginTop: 20,
        marginBottom: 10,
        marginHorizontal: 15,
    },
    portofolioCardHeader: {
        backgroundColor: '#E0E0E0',
        flex: 2,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    contactCard: {
        flex: 1,
        backgroundColor: '#F8F8F8',
        borderRadius: 10,
        elevation: 4,
        marginBottom: 10,
        marginHorizontal: 15
    },
    cardTitle: {
        fontSize: 35,
        fontFamily: 'Rancho-Regular'
    },
    portofolioBody: {
        backgroundColor: '#F8F8F8',
        flex: 5,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    portofolioContent: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    portofolioElement: {
        alignItems: 'center',
        marginHorizontal: 40
    },
    headerColor: {
        backgroundColor: '#E0E0E0',
        width: 1.5,
        height: 100
    },
    portofolioIcon: {
        height: 65,
        width: 70
    },
    contactCardHeader: {
        backgroundColor: '#F8F8F8',
        flex: 2,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    line: {
        backgroundColor: '#E0E0E0',
        height: 2
    },
    contactCardBody: {
        backgroundColor: '#F8F8F8',
        flex: 5,
        borderRadius: 10
    },
    contactList: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    contactLists: {
        height: '100%',
        justifyContent: 'space-evenly',
        marginLeft: 10,
        paddingBottom: 5
    },
    contentContactCardBody: {
        flexDirection: 'row',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default AboutScreen;