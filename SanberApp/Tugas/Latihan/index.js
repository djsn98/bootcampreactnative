import React, { useState } from 'react';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import AboutScreen from './screens/AboutScreen';
import { LoginProvider } from './data/provider';

const ScreensNavigation = () => {
    const [selectScreen, setSelectScreen] = useState('Login');

    if (selectScreen === 'Login') {
        return (
            <LoginProvider>
                <LoginScreen screenHandler={setSelectScreen} />
            </LoginProvider>
        )
    } else if (selectScreen === 'Register') {
        return <RegisterScreen screenHandler={setSelectScreen} />
    } else if (selectScreen === 'About') {
        return (
            <LoginProvider>
                <AboutScreen />
            </LoginProvider>
        )
    }
}

export default ScreensNavigation;