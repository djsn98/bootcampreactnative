import React, { useState, createContext } from 'react';
import { users } from './data';

export const UsersContext = createContext();
export const LoginContext = createContext();

export const UsersProvider = ({ children }) => {
    const [dataUsers, setDataUsers] = useState(users);

    return (
        <UsersContext.Provider value={[dataUsers, setDataUsers]}>
            {children}
        </UsersContext.Provider>
    );
};

export const LoginProvider = ({ children }) => {
    const [index, setIndex] = useState(0);

    return (
        <LoginContext.Provider value={[index, setIndex]}>
            {children}
        </LoginContext.Provider>
    );
};
