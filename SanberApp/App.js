import React from 'react';
import { StyleSheet } from 'react-native';
import { UsersProvider } from './Tugas/Tugas13/data/provider'

import Telegram from './Tugas/Tugas12/Telegram';
import Flexbox from './HandsOn/Materi-13/Flexbox';
import Props from './Tugas/Tugas13/Props';
import LoginScreen from './Tugas/Tugas13/screens/LoginScreen';
import RegisterScreen from './Tugas/Tugas13/screens/RegisterScreen';
import AboutScreen from './Tugas/Tugas13/screens/AboutScreen';
import RestApi from './Tugas/Tugas14/RestApi';
import Login from './Tugas/Tugas15/Pages/Login';
import Tugas15 from './Tugas/Tugas15/index';

import Index from './Quiz13/index';

import MovieScreen from './HandsOn/Materi-16/MovieScreen';

import ScreensNavigation from './Tugas/Latihan/index';

export default function App() {
  return (
    <>
      {/* <Telegram /> */}
      {/* <Flexbox /> */}
      {/* <Props /> */}
      {/* <LoginScreen /> */}
      {/* <RegisterScreen /> */}
      {/* <AboutScreen /> */}
      {/* <RestApi /> */}
      {/* <Tugas15 /> */}
      {/* <Index /> */}
      {/* <MovieScreen /> */}
      {/* <UsersProvider>
        <ScreensNavigation />
      </UsersProvider> */}
    </>
  );
};

const styles = StyleSheet.create({});