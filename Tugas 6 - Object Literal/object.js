//Soal No.1 (Array to Object)
function arrayToObject(arr) {
    if(arr.length === 0) {
        console.log("Error")
        return;
    }

    var now = new Date();
    var thisYear =  now.getFullYear();
    var i = 1;


    for(let userDataArray of arr) {
        let userDataObject = {
            firstName: userDataArray[0],
            lastName: userDataArray[1],
            gender: userDataArray[2],
        }

        if (userDataArray[3] == undefined || userDataArray[3] > thisYear) {
            userDataObject.age = "Invalid birth year";
        } else {
            userDataObject.age = thisYear - userDataArray[3];
        }
 
        console.log(`${i}. ${userDataObject.firstName} ${userDataObject.lastName} : {`) 
        console.log(`   firstName: ${userDataObject.firstName},`)
        console.log(`   lastName: ${userDataObject.lastName},`)
        console.log(`   gender: ${userDataObject.gender},`)
        console.log(`   age: ${userDataObject.age}`)
        console.log(`}`)
        i++
    }
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]];
arrayToObject(people);


var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]];
arrayToObject(people2);

arrayToObject([]);


// //Soal No.2 (Shopping Time)
function shoppingTime(memberId, money) {
    var itemSale = {
        "Sepatu Stacattu": 1500000,
        "Baju Zoro": 500000,
        "Baju H&N": 250000,
        "Sweater Uniklooh": 175000,
        "Casing Handphone": 50000
    }

    if (memberId == undefined || memberId == ""){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup";
    } else {
        let transaction = {
            memberId: memberId,
            money: money,
            listPurchased: [],
            changeMoney: money
        };

        for (let item in itemSale){
            if (itemSale[item] <= money){
                transaction.listPurchased.push(item)
                transaction.changeMoney -= itemSale[item];
            }
        }

        return transaction;
    }
}

//TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());


//Soal No.3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
    let rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let arrObjPenumpang = []

    for(let penumpang in arrPenumpang){
        var biaya = 0
        for(let i in rute){
            if(rute[i] == arrPenumpang[penumpang][1]) {
                biaya = i;
            } else if(rute[i] == arrPenumpang[penumpang][2]) {
                biaya = Math.abs((i - biaya) * 2000);
            }
        }

        arrObjPenumpang.push({
            penumpang: arrPenumpang[penumpang][0],
            naikDari: arrPenumpang[penumpang][1],
            tujuan: arrPenumpang[penumpang][2],
            bayar: biaya
        })
    }

    return arrObjPenumpang;
}

console.log([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]);












