//Soal No.1 Mengubah fungsi menjadi fungsi arrow
const golden = () => {
    console.log("this is golden");
}

//Soal No.2 Sederhanakan menjadi Object literal di ES6
const newFunction = function literal(firstName, lastName) {
    const fullName = () => {
        console.log(firstName + " " + lastName);
        return
    }

    return {
        firstName,
        lastName,
        fullName
    }
}

//Driver Code
newFunction("William", "Imoh").fullName();

//Soal No.3 Destructuring
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject;

//Driver code
console.log(firstName, lastName, destination, occupation, spell);

//Soal No.4 Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Magie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined)

//Soal No.5 Template Literals
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, 
    consectetur adipiscing elit, ${planet} do eiusmod tempor 
    incididunt ut labore et dolore magna aliqua. Ut enim
     ad minim veniam`

//Driver Code
console.log(before);