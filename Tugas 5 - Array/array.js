// Soal No. 1 (Range)
function range(startNum, finishNum) {
    var rangeNum = [];

    if((startNum == undefined) || (finishNum == undefined)) {
        return -1;
    }

    if(startNum < finishNum) {
        
        for (var i = startNum; i <= finishNum; i++) {
            rangeNum.push(i);
        }
    
    } else if(startNum > finishNum) {

        for (var i = startNum; i >= finishNum; i--) {
            rangeNum.push(i);
        }
 
    }

    return rangeNum;
}

console.log(range(2, 10));

//Soal No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
    var rangeNum = [];

    if((startNum == undefined) || (finishNum == undefined)) {
        return -1;
    }

    if(startNum < finishNum) {
        
        for (var i = startNum; i <= finishNum; i += step) {
            rangeNum.push(i);
        }
    
    } else if(startNum > finishNum) {

        for (var i = startNum; i >= finishNum; i -= step) {
            rangeNum.push(i);
        }
 
    }

    return rangeNum;
}

console.log(rangeWithStep(29, 2, 4));

//Soal No.3 (Sum of Range)
function sum(startNum, finishNum, step) {
    if(step == undefined) {
        let rangeNum = range(startNum, finishNum)
        let sum = 0;

        for (let i = 0; i < rangeNum.length; i++) {
            sum += rangeNum[i];
        }

        return sum;
    }

    let rangeNum = rangeWithStep(startNum, finishNum, step)
    let sum = 0;

    for (let i = 0; i < rangeNum.length; i++) {
        sum += rangeNum[i];
    }

    return sum;
}

console.log(sum(5, 50, 2));

//Soal No.4 (Array Multidimensi)
function dataHandling(dataArray) {
    for (let i = 0; i < dataArray.length; i++) {
        let userData = dataArray[i]
        console.log(`Nomor ID: ${userData[0]}`);
        console.log(`Nama Lengkap: ${userData[1]}`);
        console.log(`TTL: ${userData[2]} ${userData[3]}`);
        console.log(`Hobi: ${userData[4]} \n`);
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input);

//Soal No.5 (Balik Kata)
function balikKata(text) {
    let hasil = "";
    for (let i = text.length - 1; i >= 0; i--) {
        hasil += text[i];
    }
    console.log(hasil);
}

balikKata("Dennis");

//Soal No.6 (Metode Array)
function dataHandling2(dataArray) {
    //1
    for (let i = 0; i < dataArray.length; i++) {
        if(i == 1) {
            dataArray[i] += " Elsharawy";
        } else if(i == 2) {
            dataArray[i] = "Provinsi " + dataArray[i];
        }
    }
    dataArray.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(dataArray);

    let dateArray = dataArray[3].split("/");

    function sortDate() {
        let copyDateArray = dateArray.slice()
    
        for (let i = 0; i < copyDateArray.length; i++) {
            copyDateArray[i] = Number(copyDateArray[i]);
        }

        copyDateArray.sort(function name(value1, value2) { return value1 - value2 });
        console.log(copyDateArray);
    }

    switch (dateArray[1]) {
        case "01":
            console.log("Januari");
            sortDate();
            console.log(dateArray.join("-"));  
            console.log(dataArray[1].slice(0, 15));
            break;
        case "02":
            console.log("Februari");
            sortDate();
            console.log(dateArray.join("-"));  
            console.log(dataArray[1].slice(0, 15));
            break;
        case "03":
            console.log("Maret");
            sortDate();
            console.log(dateArray.join("-"));  
            console.log(dataArray[1].slice(0, 15));
            break;
        case "04":
            console.log("April");
            sortDate();
            console.log(dateArray.join("-"));  
            console.log(dataArray[1].slice(0, 15));
            break;
        case "05":
            console.log("Mei");
            sortDate();
            console.log(dateArray.join("-"));  
            console.log(dataArray[1].slice(0, 15));
            break;
        case "06":
            console.log("Juni");
            sortDate();
            console.log(dateArray.join("-"));
            console.log(dataArray[1].slice(0, 15));
            break;
        case "07":
            console.log("Juli");
            sortDate();
            console.log(dateArray.join("-"));  
            console.log(dataArray[1].slice(0, 15));
            break;
        case "08":
            console.log("Agustus");
            sortDate();
            console.log(dateArray.join("-")); 
            console.log(dataArray[1].slice(0, 15)); 
            break;
        case "09":
            console.log("September");
            sortDate();
            console.log(dateArray.join("-")); 
            console.log(dataArray[1].slice(0, 15)); 
            break;
        case "10":
            console.log("Oktober");
            sortDate();
            console.log(dateArray.join("-")); 
            console.log(dataArray[1].slice(0, 15)); 
            break;
        case "11":
            console.log("November");
            sortDate();
            console.log(dateArray.join("-"));  
            console.log(dataArray[1].slice(0, 15));
            break;
        case "12":
            console.log("Desember");
            sortDate();
            console.log(dateArray.join("-"));  
            console.log(dataArray[1].slice(0, 15));
            break;
        default:
            console.log("Salah memasukan angka bulan!!");
            console.log(dateArray.join("-"));  
            console.log(dataArray[1].slice(0, 15));
            break;  
    }
}

dataHandling2(["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]);




